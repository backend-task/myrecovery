from django.urls import path

from .views import TypesView, SpecialitiesView, SurgoensListView, SurgoenDetailView, Surgoen_TeamListView
# , NursesListView, Admin_AssitantsListView, NurseDetailView, Admin_AssitantDetailView

urlpatterns = [
    path('types/', TypesView.as_view()),
    path('specialities/', SpecialitiesView.as_view()),
    path('surgoens/', SurgoensListView.as_view()),
    path('surgoenteams/', Surgoen_TeamListView.as_view()),
    # path('nurses/', NursesListView.as_view()),
    # path('adminassitants/', Admin_AssitantsListView.as_view()),
    path('surgoens/<int:pk>/', SurgoenDetailView.as_view()),
    # path('nurses/<int:pk>/', NurseDetailView.as_view()),
    # path('adminassitants/<int:pk>/', Admin_AssitantDetailView.as_view()),
]
