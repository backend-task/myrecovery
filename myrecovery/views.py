from rest_framework.views import APIView
from rest_framework.response import Response
# Create your views here.

from .models import Type, Speciality, Surgoen, Surgoen_Team
from .serializers import TypeSerializer, SpecialitySerializer, SurgoenSerializer, SurgoenSerializerPopulated, Surgoen_TeamSerializerPopulated, Surgoen_TeamSerializer

class TypesView(APIView):

    def get(self, _request):
        types = Type.objects.all()
        serializer = TypeSerializer(types, many=True)
        return Response(serializer.data)

class SpecialitiesView(APIView):

    def get(self, _request):
        specialities = Speciality.objects.all()
        serializer = SpecialitySerializer(specialities, many=True)
        return Response(serializer.data)

    def post(self, request):
        serializer = SpecialitySerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=201)
        return Response(serializer.errors, status=422)

class SurgoensListView(APIView):

    def get(self, _request):
        surgoens = Surgoen.objects.all()
        serializer = SurgoenSerializerPopulated(surgoens, many=True)
        return Response(serializer.data)

    def post(self, request):
        serializer = SurgoenSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=201)
        return Response(serializer.errors, status=422)

class SurgoenDetailView(APIView):
    def get(self, _request, pk):
        surgoens = Surgoen.objects.get(pk=pk)
        serializer = SurgoenSerializerPopulated(surgoens)
        return Response(serializer.data)

    def put(self, request, pk):
        surgoens = Surgoen.objects.get(pk=pk)
        serializer = SurgoenSerializer(surgoens, data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data)
        return Response(serializer.errors, status=422)

class Surgoen_TeamListView(APIView):
    def get(self, _request):
        surgoens_team = Surgoen_Team.objects.all()
        serializer = Surgoen_TeamSerializerPopulated(surgoens_team, many=True)
        return Response(serializer.data)

    def post(self, request):
        serializer = Surgoen_TeamSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=201)
        return Response(serializer.errors, status=422)
