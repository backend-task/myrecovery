from rest_framework import serializers
from .models import Type, Speciality, Surgoen, Nurse, Admin_Assitant, Surgoen_Team


class TypeSerializer(serializers.ModelSerializer):

    class Meta:
        model = Type
        fields = ('id', 'name')

class SpecialitySerializer(serializers.ModelSerializer):

    class Meta:
        model = Speciality
        fields = ('id', 'name')

class SurgoenSerializer(serializers.ModelSerializer):

    class Meta:
        model = Surgoen
        fields = ('id', 'profilePicture', 'firstName', 'lastName', 'type', 'onLeave', 'specialities', 'biography')

class SurgoenSerializerPopulated(serializers.ModelSerializer):

    type = TypeSerializer()
    specialities = SpecialitySerializer()
    class Meta:
        model = Surgoen
        fields = ('id', 'profilePicture', 'firstName', 'lastName', 'type', 'onLeave', 'specialities', 'biography')

class NurseSerializer(serializers.ModelSerializer):

    class Meta:
        model = Nurse
        fields = ('id', 'profilePicture', 'firstName', 'lastName', 'type', 'onLeave', 'biography')

class Admin_AssitantSerializer(serializers.ModelSerializer):

    class Meta:
        model = Admin_Assitant
        fields = ('id', 'profilePicture', 'firstName', 'lastName', 'type', 'onLeave', 'biography')



class Surgoen_TeamSerializer(serializers.ModelSerializer):

    class Meta:
        model = Surgoen_Team
        fields = ('id', 'surgoen', 'admin_assitant', 'nurses')
class Surgoen_TeamSerializerPopulated(serializers.ModelSerializer):


    surgoen = SurgoenSerializerPopulated()
    admin_assitant = Admin_AssitantSerializer()
    nurses = NurseSerializer(many=True)

    class Meta:
        model = Surgoen_Team
        fields = ('id', 'surgoen', 'admin_assitant', 'nurses')
