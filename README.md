# myrecovery

## Building the models in the beginning

When I started the task, I read the brief and decided to start with a few simple models - the type of the staff member, for surgeons, their speciality and a general staff member model.
I spent about two hours on building everything, models, views, serializers, admin look and some seeds for the database.
Only after that, I started to build the model of the team. That was when I understood that my whole model is wrong, and I need to rebuild everything.


## Changing the models
I decided to change the whole way I build the staff members, and not creating them in one model, but to each role to make a model. That gave me the option to build the team model with some limitation - unique surgeon to each team and not more than one surgeon and one admin assistant in each team.

## What I succeeded according to the briefing
I will start from the endpoint - for each staff member you can see all the fields I was requested to show. Only for surgeons, I also added the speciality field. Each one also has a type, although the models are different so you can view them in lists according to their type. That will allow me to extend the views also to whole staff members.

I built a team model that must have a unique surgeon. It means that every team has a surgeon, and no surgeon can work in other teams. Also, each team must have at least one nurse (but can also have more than one). A team can also have an admin assistant, but it is not obligatory.

There can be only one surgeon and one admin assistant in each team. The nurses filed was build as a list so there can be as many nurses as we want.

## Backend user-friendly
I used Django rest framework, which gives us a user-friendly way to add staff in general and to build teams.
I also added to option to add specialities, since I do not know all the specialities which exist, but I did not add the option to add types since I see it as a closed list (surgeon, nurse and admin assistant).

I created views only for part of the models, because of the short time, so the admin (superuser) can add and edit to any of the models, but the users, for now, can add and edit only the teams and the surgeons. I did not add the delete option since I think it should be only by admin, and not by users. I can think about an option to add it also for specific users with the right authentication process.

## What I did not manage to do
I started to build a test, but I ran out of time, I comment it out, so you could see what I started. It is for sure, not a full test. Another thing I never did and I could not find an answer in the time frame was how to limit the number of times I use the nurses and the admin assistant in teams.

## Timeframe
I used all four hours. I first read the assignment at noon but started to work on it only in the late afternoon. I did not count in these four hours the time that I used to open a user in Gitlab and writing the readme.

Thank you for the opportunity to show you my ability to work with Django.

Daniel
